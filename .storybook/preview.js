import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/assets/libs/Fontawsome';
import '../src/assets/css/index.css';
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}