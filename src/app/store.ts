import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import authReducer from '../slices/auth.slice';
import offreReducer from '../slices/offre.slice';

export const store = configureStore({
  reducer: {
    _auth : authReducer,
    _offre : offreReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
