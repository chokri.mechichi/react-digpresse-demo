import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../app/store';


export interface AuthState {
    loading: boolean 
    data: any   // it will containe the payload of API response (success)
    error: string | string[]  // it will containe the payload of API error msg ( fail)
}

const initialState: AuthState = {
    loading: false,
    data: null,
    error: ''
};

export const authSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers : {
        clear: (state) => {
            state = initialState
        },
    } ,
    extraReducers : {}
})

export const { clear } = authSlice.actions;
export default authSlice.reducer;