import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../app/store';
import { fetchOffre } from '../services/offre.service';
import { Presse } from '../types/entity/Presse';


export const getOffreList = createAsyncThunk(
    'Offre/fetch-list',
    async (data, { rejectWithValue }) => {
        try {
            const res = await fetchOffre();
            return res.data;
        } catch (error: any) {
            const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
            return rejectWithValue(message)
        }
    }
);

export interface OffreState {
    loading: boolean
    data: any   // it will containe the payload of API response (success)
    error: string | string[]  // it will containe the payload of API error msg ( fail)
    data_list : Presse[]
}

const initialState: OffreState = {
    loading: false,
    data: null,
    error: '',
    data_list : []
};

export const offreSlice = createSlice({
    name: 'offre',
    initialState,
    reducers: {
        clear: (state) => {
            state = initialState
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getOffreList.pending, (state) => {
                state.loading = true;
                state.error = '';
            })
            .addCase(getOffreList.fulfilled, (state, action) => {
                state.loading = false;
                state.data_list = action.payload as Presse[];
            })
            .addCase(getOffreList.rejected, (state , action) => {
                state.loading = false;
                state.error = action.payload as string;
            });
    }
})

//SELECT STATES HELPER
export const selectLoading = (state: RootState) => state._offre.loading;
export const selectOffreList = (state: RootState) => state._offre.data_list as Presse[];
export const selectError = (state: RootState) => state._offre.error;

export const { clear } = offreSlice.actions;
export default offreSlice.reducer;