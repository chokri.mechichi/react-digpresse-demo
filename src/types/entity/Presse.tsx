export type Presse = {
    id : number
    nom : string
    description : string
    presse_type : string[]
    logo : string
    audience : number
}