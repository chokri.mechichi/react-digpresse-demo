import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button , {ButtonVariant} from '../../atoms/button/Button';
import Inpute from '../../atoms/input/Inpute';
import styles from './Style.module.css'
import classNames from 'classnames';

const PresseHeaderList = () => {
    return (
        <div className="row m-0 home-banner bg-white" >
            <div className="col-md-12  p-2">
                <div className="row m-0">
                    <div className="col-md-12 align-items-end dvdr-b-gray pb-2">
                        <div className="d-inline-block w-auto pt-1">
                            <b className="icon-item-round">
                                <FontAwesomeIcon icon={['fas', 'shop']} />
                            </b>
                        </div>
                        <h5 className="d-inline-block txt-secondary ps-2 pt-2 mb-0 ">MARKET</h5>

                        <Button theme='btn-success' className='float-end'><FontAwesomeIcon icon={['fas', 'plus']} />  Créer un offre</Button>
                    </div>

                    <div className="col-md-6  pt-2 pb-1 offset-md-3 text-center">
                        <h3 className="text-center">Explorer nos offres de presse</h3>
                        <Inpute _type='text' className='w-75 d-inline-block py-1 no-border-rad' name='search' placeholder='Search ...' />
                        <Button theme={ButtonVariant.PRIMARY} className={classNames(styles.btn_search ,'btn-sm no-border-rad')}><FontAwesomeIcon icon={['fas', 'search']} /></Button>

                    </div>


                </div>
            </div>

        </div>
    );
};

export default PresseHeaderList;