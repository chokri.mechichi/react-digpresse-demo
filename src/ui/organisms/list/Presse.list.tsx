import React from 'react';
import { Presse } from '../../../types/entity/Presse';
import PresseItem from '../../molecules/item/Presse.item';

type Props = {
    data_list : Presse[]
    onItemClick : (selectedItem : Presse)=>void
}

const PresseList = (props : Props) => {
    const {data_list , onItemClick} = props
    return (
        <div className='row m-0 p-3'>
            {data_list.map(el => <PresseItem data={el} onItemClick={onItemClick} key={`p-itm-${el?.id}`} /> )}
        </div>
    );
};

const defaultProps = {
    data_list : [] ,
    onItemClick : (selectedItem: Presse)=>{}
}

PresseList.defaultProps = defaultProps ;

export default PresseList;