import React from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Dropdown from '../../atoms/dropdown/Dropdown';
import styles from './Style.module.css'


type Props = {
    isOpen: boolean
    onToggle: (s: boolean) => void

}
const Navbar = (props: Props) => {
    const { isOpen, onToggle } = props
    return (
        <nav className={classNames( styles["hd-navbar"] ,"fixed-top" , {[styles["hd-navbar-mini"]] : !isOpen})}>
            <button className={styles["sidebar-toggle"]} data-toggle="collapse" data-target=".main-sidebar" role="button"
                onClick={() => onToggle(!isOpen)}>
                <FontAwesomeIcon icon={['fas', 'bars']} />
            </button>

            <div className="pr-4" style={{ float: "right", marginRight: "24px", display: "inline-block" }}>
                <ul className="nav navbar-nav" style={{ flexDirection: "row" }}>

                    <li >
                        <Dropdown toggleChildren={
                            <b className="fnt-w5 ">
                                <span>
                                    <FontAwesomeIcon icon={['fas', 'user-circle']} />
                                </span>
                                <span className="fnt-med ps-2">{`Chokri Mechichi`}</span>
                            </b>
                        }>
                            <a href="#">Profile</a>
                            <a href="#">Logout</a>
                        </Dropdown>
                    </li>

                    <li className="ps-3 dvdr-l-gray">
                        <Dropdown 
                        toggleClassName={classNames('p-0' , styles['btn_round_notif'])}
                        toggleChildren={
                            <>
                                <FontAwesomeIcon icon={['fas', 'bell']} />
                                <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger fnt-sm mt-2">
                                +99
                                </span>
                            </>
                        }>
                            <a href="#">Hellow</a>
                        </Dropdown>
                    </li>


                </ul>
            </div>


        </nav>
    );
};

export default Navbar;