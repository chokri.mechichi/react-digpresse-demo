import React from 'react';
import MenuNavLink from '../../molecules/nav/NavLink';
import classNames from 'classnames';
import styles from './Style.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const links = [
    {
        label: 'Market',
        icon: 'store',
        link: '/'
    },
    {
        label: 'Campagnes',
        icon: 'bullhorn',
        link: '/comp'
    },
    {
        label: 'Profile',
        icon: 'user',
        link: '/profile'
    },
    {
        label: 'Factures',
        icon: 'file-invoice-dollar',
        link: 'bills'
    },
]

type Props = {
    isOpen: boolean
}

const Sidebar = (props: Props) => {
    const { isOpen } = props
    const dologout = ()=>{

    }
    return (

        <aside className={classNames(styles['main-sidebar'], { [styles['mini-sidebar']]: !isOpen })} >
            <section className="sidebar">

                <div className="col-md-12 py-4" style={{ background: '#fff' }}>
                    <a href={`/chef/`} className={styles.sidebar_logo}>
                        <img src='/logo.png' alt="" />
                    </a>
                </div>

                <ul className={classNames(styles['sidebar-menu'], 'mt-4 pt-4')} >
                    {
                        links.map(el => <MenuNavLink label={el.label} icon={el.icon} link={el.link} />)
                    }
                </ul>

                <ul>
                    <li className={styles["cont_log_out"]}>
                        <button className={styles["btn_log_out"]} onClick={dologout}>
                            <FontAwesomeIcon icon={['fas', 'sign-out-alt']} />
                        </button>
                    </li>
                </ul>
            </section>
        </aside>
    );
};

export default Sidebar;