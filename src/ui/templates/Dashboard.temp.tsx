import React from 'react';

type Props = {
    sideNav: React.ReactNode
    topNav: React.ReactNode
    children?: React.ReactNode
    isOpen: boolean
}
const DashboardTemp = ({ sideNav, topNav, children, isOpen }: Props) => {
    return (
        <>
            {sideNav}
            {topNav}

            <div className={isOpen ? "rw-cont" : "rw-cont rw-cont-mini"}>
                {children}
            </div>
        </>
    );
};

export default DashboardTemp;