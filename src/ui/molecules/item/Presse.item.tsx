import React from 'react';
import { Presse } from '../../../types/entity/Presse';
import Card from '../../atoms/card/Card';
import Button from '../../atoms/button/Button';
import Iconlabel from '../../atoms/label/Iconlabel';
import styles from './Style.module.css'
import classNames from 'classnames';

type Props = {
    data: Presse
    onItemClick: (selectedItem: Presse) => void
}
const PresseItem = (props: Props) => {
    const { data, onItemClick } = props
    return (
        <Card className='col-md-12 col-12 p-0'>
            <div className="row m-0 ">

                {/*---------------------- Presse Image --------------------------------------------*/}
                <div className="col-md-2 p-0">
                    <img className={styles['item-presse-img']} src={`/img/${data.logo}`} width='80%' alt="" loading='lazy' />
                </div>

                <div className="col-md-8 py-2">

                    {/*---------------------- Presse Title --------------------------------------------*/}
                    <h5 className='fnt-w7'>{data.nom}</h5>

                    {/*---------------------- Presse Descript --------------------------------------------*/}
                    <p className='text-muted m-b-2 '>{data.description}</p>

                    <div className="row m-0">

                        {/*---------------------- Presse types--------------------------------------------*/}
                        <div className="col-md-6 ps-0">
                            <Iconlabel content='Type' icon='bullhorn' />
                            {
                                data.presse_type.map(el => <span className="badge bg-dark me-1">{el}</span>)
                            }
                        </div>

                        {/*---------------------- Presse audience --------------------------------------------*/}
                        <div className={classNames('col-md-6 dvdr-l-gray' , styles['aud-container'])}>
                            <Iconlabel content='Audience' icon='assistive-listening-systems' />
                            <b className='d-block fnt-w7'>{String(data.audience).substring(0, 2)}M</b>
                        </div>
                    </div>
                </div>

                <div className="col-md-2 text-end p-2 ">
                    <Button theme='btn-primary' type='button' className='w-100 py-2' onClick={() => onItemClick(data)}>Lancer une campagne</Button>
                </div>
            </div>
        </Card>
    );
};

const defaultProps = {
    onItemClick: (selectedItem: Presse) => { }
}

PresseItem.defaultProps = defaultProps;

export default PresseItem;