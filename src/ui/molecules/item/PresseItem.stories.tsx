import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import PresseItem from './Presse.item';
import { Presse } from '../../../types/entity/Presse';
import { data} from '../../../services/fake-data';

export default {
    title: 'Molecules/Items',
    component: PresseItem,
  } as ComponentMeta<typeof PresseItem>;

  const Template: ComponentStory<typeof PresseItem> = (args) => <PresseItem {...args} />;

  export const Presse_item = Template.bind({});
  Presse_item.args = {
    data : data[0] as Presse
  };


