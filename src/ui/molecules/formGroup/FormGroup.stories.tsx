import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import FormGroup from './FormGroup';
import Inpute from '../../atoms/input/Inpute';

export default {
    title: 'Molecules/Form',
    component: FormGroup,
  } as ComponentMeta<typeof FormGroup>;

  const Template: ComponentStory<typeof FormGroup> = (args) => <FormGroup {...args} />;

  export const Form_Group = Template.bind({});
  Form_Group.args = {
    label : 'E-mail',
    children : <Inpute _type='email' className='w-50 brd-rad-4' name='email' placeholder='Email...'  />
  };
