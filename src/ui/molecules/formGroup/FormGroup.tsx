import React from 'react';
import Form from 'react-bootstrap/Form';

type Props = {
    label : string
    children : React.ReactNode
    className? : string
    error_msg? : string
    hint_msg? : string
}

const FormGroup = ({label , children , className , error_msg , hint_msg} : Props) => {
    return (
        <Form.Group className={className} >
            <Form.Label className='fnt-w5 mb-1 ps-1'>{label}</Form.Label>
            {children}
            {/* -----------------Display validation error msg---------------------------------------- */}
            <Form.Text className="text-danger">
                {error_msg}
            </Form.Text>
            {/* -----------------Display hint info msg---------------------------------------- */}
            <Form.Text className="text-muted">
                {hint_msg}
            </Form.Text>
        </Form.Group>
    );
};

export default FormGroup;