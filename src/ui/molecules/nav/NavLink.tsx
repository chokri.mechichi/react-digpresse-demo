import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Styles from "./Style.module.css";
import classNames from "classnames";
type Props = {
    label: string
    icon: string
    link: string
}
const MenuNavLink = (props: Props) => {
    const { label, icon, link } = props
    const classprops: string = classNames()
    return (
        <li className={Styles['sidebar-menu-item']}>
            <NavLink to={link}>
                <span className={Styles['icon']}><FontAwesomeIcon icon={['fas', icon as any]} /></span>
                <span className={Styles['label']}>{label}</span>
            </NavLink>
        </li>
    )
}

MenuNavLink.defaultProps = {
    label: 'Menu-Link',
    icon: 'home',
    link: '#'
}

export default MenuNavLink;