import React from 'react';
import classnames from 'classnames'
import styles from './Style.module.css'

export const ButtonType = {
    BUTTON: 'button',
    RESET: 'reset',
    SUBMIT: 'submit',
}
export const ButtonVariant = {
    DEFAULT: 'btn-default',
    PRIMARY: 'btn-primary',
    SUCCESS: 'btn-success',
    WARNING: 'btn-warning',
}
export const ButtonTheme = {
    DEFAULT: 'default',
    ROUNDED: 'rounded',
}

export const ButtonSize = {
    SMALL: 'small',
    MEDIUM: 'medium',
    LARGE: 'large',
}

type Props = {
    type: 'button' | 'reset' | 'submit',
    theme: string,
    size: string,
    onClick: () => void,
    children: React.ReactNode,
    className: string,
    disabled: boolean,
    loading: boolean,
}

const Button = (props: Props) => {
    const { type, onClick, children, theme, size, className, disabled, loading } = props
    const classProps: string = classnames('btn',styles.btn_eff, styles[theme], styles[size], { [styles.disabled]: disabled }, className)

    return (
        <button type={type} onClick={onClick} disabled={disabled} className={classProps}>
            
            {children}
            {
                loading &&
                <div className="spinner-border spinner-border-sm text-light float-end mt-1" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            }
        </button>
    )
}

Button.defaultProps = {
    type: ButtonType.BUTTON,
    theme: ButtonTheme.DEFAULT,
    size: ButtonSize.MEDIUM,
    onClick: () => { },
    className: '',
    disabled: false,
    loading: false,
}

export default Button;