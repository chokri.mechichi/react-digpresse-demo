import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Button from './Button';
import {ButtonVariant} from './Button';

export default {
    title: 'Atoms/Button',
    component: Button,
  } as ComponentMeta<typeof Button>;

  const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

  export const Primary = Template.bind({});
  Primary.args = {
    theme: ButtonVariant.PRIMARY,
    children : 'Button ',
  };


  export const Success = Template.bind({});
  Success.args = {
    theme: ButtonVariant.SUCCESS,
    children : 'Button ',
  };


  export const Warning = Template.bind({});
  Warning.args = {
    theme: ButtonVariant.WARNING,
    children : 'Button ',
  };