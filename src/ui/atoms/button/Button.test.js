import Button , {ButtonVariant} from "./Button";
import { render } from "@testing-library/react";

describe("Primary Button", () => {
  it("Renders Primary Button", () => {
    // ARRANGE
    const wrapper = render(<Button theme={ButtonVariant.PRIMARY} >button</Button>)
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });
  
})