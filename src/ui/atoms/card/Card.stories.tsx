import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Card from './Card';

export default {
    title: 'Atoms/Card',
    component: Card,
  } as ComponentMeta<typeof Card>;

  const Template: ComponentStory<typeof Card> = (args) => <Card {...args} />;

  export const card = Template.bind({});
  card.args = {
    children : <h2>Card container</h2>,
  };
