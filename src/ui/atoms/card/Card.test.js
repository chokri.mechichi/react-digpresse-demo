import Card from "./Card";
import { render } from "@testing-library/react";

describe("Card", () => {
  it("Renders card", () => {
    // ARRANGE
    const wrapper = render(<Card>hello</Card>)
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });
  
})