import React from 'react';
import classnames from 'classnames'
import styles from './Style.module.css'


type Props = {
    theme?: string,
    onClick?: () => void,
    children?: React.ReactNode,
    className?: string,
} & typeof defaultProps;

const Card = (props : Props) => {
    const { theme  , className , children} = props
    const classProps: string = classnames('card my-1',styles.card, styles[theme] , className)

    return (
        <div className={classProps}>
            {children}
        </div>
    );
};

const defaultProps = {
    theme : 'light',
    onClick: () => {},
    className : ''
}
Card.defaultProps = defaultProps

export default Card;