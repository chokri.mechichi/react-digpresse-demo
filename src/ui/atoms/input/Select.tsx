import React from 'react';
import Form from 'react-bootstrap/Form';

export type OptionKeyValue = {
        key : string
        value : string | number
}
type Props = {
    value?: string | number
    placeholder?: string
    onChangeHandler: (e: React.ChangeEvent<HTMLSelectElement>) => void
    className?: string
    options : OptionKeyValue[]
}
const Select = ({ placeholder, value, onChangeHandler, className , options }: Props) => {
    return (
        <Form.Select placeholder={placeholder} value={value} onChange={onChangeHandler} className={className}>
            <option >choose option</option>
            {
                options.map(el => <option value={el.value} >{el.key}</option>)
            }
        </Form.Select>
    );
};

const defaultProps = {
    placeholder: '',
    onChangeHandler: (e: React.ChangeEvent<HTMLSelectElement>) => { },
    className: '',
    options : []
}
Select.defaultProps = defaultProps

export default Select;