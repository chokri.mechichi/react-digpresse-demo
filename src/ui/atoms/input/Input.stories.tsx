import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Inpute from './Inpute';

export default {
    title: 'Atoms/Input',
    component: Inpute,
  } as ComponentMeta<typeof Inpute>;

  const Template: ComponentStory<typeof Inpute> = (args) => <Inpute {...args} />;

  export const inpute = Template.bind({});
  inpute.args = {
    _type : 'text',
    placeholder : 'Type you name'
  };
