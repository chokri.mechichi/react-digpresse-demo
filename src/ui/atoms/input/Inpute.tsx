import React from 'react';
import Form from 'react-bootstrap/Form';

type Props = {
    _type : string
    value? : string | number
    placeholder? : string
    onChangeHandler : ( e : React.ChangeEvent<HTMLInputElement>)=>void
    className? : string
    name? : string
}
const Inpute = ({_type , placeholder , value , onChangeHandler , className , name} : Props) => {
    return (
        <Form.Control type={_type} placeholder={placeholder} value={value} onChange={onChangeHandler} className={className} name={name} />
    );
};

const defaultProps = {
    _type : 'text',
    placeholder : '',
    onChangeHandler : (e :React.ChangeEvent<HTMLInputElement>)=>{},
    className : '',
    name : '',
}
Inpute.defaultProps = defaultProps

export default Inpute;