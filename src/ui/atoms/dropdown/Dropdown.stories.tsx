import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Dropdown from './Dropdown';

export default {
    title: 'Atoms/Dropdown',
    component: Dropdown,
  } as ComponentMeta<typeof Dropdown>;

  const Template: ComponentStory<typeof Dropdown> = (args) => <Dropdown {...args} />;

  export const dropdown = Template.bind({});
  dropdown.args = {
    children : <a href='#'>Link</a>,
    toggleChildren : <b>Dropdown</b>
  };
