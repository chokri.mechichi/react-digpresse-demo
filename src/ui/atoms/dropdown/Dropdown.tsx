import React from 'react';
import { Dropdown as  Dropdownbs} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';

type Props = {
    children? : React.ReactNode
    toggleChildren : React.ReactNode
    className? : string
    toggleClassName? : string
}
const Dropdown = (props : Props) => {
    const {children , toggleChildren , className , toggleClassName} = props
    return (
        <Dropdownbs drop="down" className=''>
            <Dropdownbs.Toggle variant="default  fnt-largr" className={classNames("no-arrow  no-brd-rad " , toggleClassName)} id="dropdown-basic" >
                {toggleChildren}

            </Dropdownbs.Toggle>

            <Dropdownbs.Menu style={{ width: "400px", maxHeight: "300px", overflowY: "scroll" }} className="blue_scrollb">
                {children}
            </Dropdownbs.Menu>
        </Dropdownbs>
    );
};

export default Dropdown;