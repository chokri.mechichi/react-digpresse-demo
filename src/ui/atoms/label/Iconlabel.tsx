import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type Props = {
    content: string
    icon: string
}
const Iconlabel = (props : Props) => {
    const {content , icon } = props
    return (
        <b className='d-block fnt-sm'>
            <span className='me-2 txt-primary'><FontAwesomeIcon icon={['fas', icon as any]} /></span>
            <span>{content}</span>
        </b>
    );
};

export default Iconlabel;