import React, { useState } from 'react';
import { Routes, Route } from "react-router-dom";

import Sidebar from '../../ui/organisms/menu/Sidebar';
import Navbar from '../../ui/organisms/menu/Navbar';
import DashboardTemp from '../../ui/templates/Dashboard.temp';

import ExploreOffreContainer from '../offrePresse/ExploreOffre.container';

const Main = () => {

    const [isOpen, setIsopen] = useState(true);
    const onToggle = (state: boolean) => setIsopen(state)

    const dologout = () => {
    }

    return (
        <>

            <DashboardTemp
                isOpen={isOpen}
                sideNav={<Sidebar isOpen={isOpen} />}
                topNav={<Navbar isOpen={isOpen} onToggle={onToggle} />} >
                
                <Routes>
                    <Route index element={<ExploreOffreContainer />} />
                </Routes>

            </DashboardTemp>

        </>
    );
};

export default Main;