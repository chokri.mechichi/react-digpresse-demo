import React, { useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { getOffreList, clear, selectLoading, selectOffreList, selectError } from '../../slices/offre.slice';
import { Presse } from '../../types/entity/Presse';
import PresseList from '../../ui/organisms/list/Presse.list';
import PresseHeaderList from '../../ui/organisms/list/Presse.HeaderList';

const ExploreOffreContainer = () => {

    const dispatch = useAppDispatch();
    const loading = useAppSelector(selectLoading);
    const offreList = useAppSelector(selectOffreList);
    const error = useAppSelector(selectError);

    useEffect(() => {
        dispatch(getOffreList())
    }, [])

    const onItemClick = (item: Presse) => { }

    return (
        <>
            <PresseHeaderList />
            <PresseList data_list={offreList} onItemClick={onItemClick} />
        </>
    );
};

export default ExploreOffreContainer;