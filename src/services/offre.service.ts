import { Presse } from "../types/entity/Presse";
import { API_BASE_URL, TOKEN_NAME } from "./config";
import { getItemByName } from "./Storage";
import { data } from "./fake-data"; 

export function fetchOffre() {
    return new Promise<{ data: Presse[] }>((resolve) =>
        setTimeout(() => resolve({ data: data as Presse[] }), 500)
    );
}