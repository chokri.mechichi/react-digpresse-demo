//LOCAL STORAGE HELPER

export function getItemByName(name : string) {
    return JSON.parse(localStorage.getItem(name) || '');
};
export function setItemByName(data : any , name : string) {
    localStorage.setItem(name, JSON.stringify(data));
};