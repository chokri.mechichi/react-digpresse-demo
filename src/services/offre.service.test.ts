import {fetchOffre} from './offre.service'

describe("Fetch Offre Service", () => {
  it("Return Data of Presse", async () => {
    const eventDataList = await fetchOffre();
    expect(eventDataList).toBeDefined();
  });
  
});