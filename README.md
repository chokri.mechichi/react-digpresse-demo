# React Digpresse Demo
Digital Presse Market Place demo (React , Redux , Atomic Design)

![image info](screen1.png)
## Libraries
    React v18
    React-router v6
    Redux/Reduxtoolkit
    React-bootstrap
    React-fontawesome
    Storybook v6
## Project Folder Structure
    /ui
        /_settings
        /atoms
        /molecules
        /organisms
        /templates
    /features
    /services
    /slices
## Available Scripts

Run :
### `npm start`
Storybook :
### `npm run storybook`
Test :
### `npm test`
Build
### `npm run build`